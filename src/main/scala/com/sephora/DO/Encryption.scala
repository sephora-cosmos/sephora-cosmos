package com.sephora.DO

object Encryption {

/**
 * Sample:
 * {{{
 *   scala> val key = "My very own, very private key here!"
 *
 *   scala> Encryption.encrypt(key, "pula, pizda, coaiele!")
 *   res0: String = 9R2vVgkqEioSHyhvx5P05wpTiyha1MCI97gcq52GCn4=
 *
 *   scala> Encryption.decrypt(key", res0)
 *   res1: String = pula, pizda, coaiele!
 * }}}
 */

//  def encrypt(key: String, value: String): String = {
//    val cipher: Cipher = Cipher.getInstance("AES/ECB/PKCS5Padding")
//    cipher.init(Cipher.ENCRYPT_MODE, keyToSpec(key))
//    Base64.encodeBase64String(cipher.doFinal(value.getBytes("UTF-8")))
//  }
//
//  def decrypt(key: String, encryptedValue: String): String = {
//    val cipher: Cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING")
//    cipher.init(Cipher.DECRYPT_MODE, keyToSpec(key))
//    new String(cipher.doFinal(Base64.decodeBase64(encryptedValue)))
//  }
//
//  def keyToSpec(key: String): SecretKeySpec = {
//    var keyBytes: Array[Byte] = (SALT + key).getBytes("UTF-8")
//    val sha: MessageDigest = MessageDigest.getInstance("SHA-1")
//    keyBytes = sha.digest(keyBytes)
//    keyBytes = util.Arrays.copyOf(keyBytes, 16)
//    new SecretKeySpec(keyBytes, "AES")
//  }
//
//  private val SALT: String =
//    "jMhKlOuJnM34G6NHkqo9V010GhLAqOpF0BePojHgh1HgNg8^72k"
  
  
  
def encryptString(value:String):String = {
  if(value !=null && value.nonEmpty) {
    value.map(x => if ((x.toByte + 2) > 90 && (x.toByte + 2) < 97) x.toByte + 9 else if ((x.toByte + 2) > 122) 64 + ((x.toByte + 2) - 122) else (x.toByte + 2)).map(x => x.toChar).mkString
  }
  else
    value
}

def encryptEmail(value:String):String = {
  if(value !=null && value.nonEmpty) {
    val split = value.split('@')
    encryptString(split(0)) + "@" + split(1)
  }
  else
    value
}
def encryptInt(value:String): Long = {
  if (value != null && value.nonEmpty) {
    val result = toLong(value) match {
      case Some(num) =>  value.map(x => if ((x.toByte + 2) > 57) 48 + ((x.toByte + 2) - 57) else (x.toByte + 2)).map(x => x.toChar).mkString.toLong
      case None => 0
    }
    result
  }
  else
    0
}

def encryptDOB(value:String):String = {
  if(value !=null && value.nonEmpty) {
    val split = value.split("/")
    encryptMonth(split(0)) + "/" + encryptDay(split(1)) + "/" + split(2)
  }
  else value
}
def encryptAddress(value:String):String = {
  if(value !=null && value.nonEmpty) {
    val pattern = "([0-9]+)".r
    val split = pattern.findAllIn(value)
    val x = split.map(x => (x, x))
    val y = x.map(x => (x._1, x._2.map(x => if ((x.toByte + 2) > 57) 48 + ((x.toByte + 2) - 57) else (x.toByte + 2)).map(x => x.toChar).mkString))
    y.foldLeft(value)((a, b) => a.replaceAllLiterally(b._1, b._2))
  }
  else
    value
}

def encryptMonth(value:String):Int = {
  if(value !=null && value.nonEmpty) {
    (value.toInt + 1) % 12
  }
  else
    0
}
def encryptDay(value:String):Int = {
  if(value !=null && value.nonEmpty) {
    (value.toInt + 6) % 28
  }
  else
    0
}

def toLong(s: String): Option[Long] = {
    try {
        Some(s.toLong)
    } catch {
        case e: NumberFormatException => None
    }
}
 
}
  
  