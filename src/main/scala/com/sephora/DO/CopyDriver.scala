package com.sephora.DO

import java.io.File

import com.sephora.schema.Entities.{Context, FileConfig}
import com.sephora.schema.{AzureStorageHelper, ConfigParser, DataFrameHelper, FileHelper}
//import java.nio.file.Path
import com.microsoft.azure.storage.blob._
import org.apache.commons.io.FilenameUtils
import org.apache.spark.sql.{SparkSession, _}
import org.apache.spark.sql.functions._


//import parquet.hadoop.ParquetFileWriter.Mode
import org.apache.commons.io.FileUtils
import org.apache.hadoop.conf._
import org.apache.hadoop.fs.{FileSystem, Path}


object CopyDriver {
  
  def main(args:Array[String]) = {
    
    val spark= SparkSession.builder()
          .appName("schema-generator")
          .master("local")
          .getOrCreate() 
            
    val path = args(0)//"D:\\Sephora\\configs\\files.conf"//
    val context = ConfigParser.parse(path)
    
    val fileConfigs =  createFileConfig(context) // loadLocalConfig(context)//
    for(file <- fileConfigs) {
      val rawDF = DataFrameHelper.buildDataFrame(file, spark)
//      rawDF.printSchema()
      //val map = getColumns(file.maskColumns)
//      println("************ file **************" + file.name)
//      println("************ masked columns **************" + file.maskColumns)
      val encryptedDF = encryptDF(file.maskColumns, rawDF)
      encryptedDF.show()
      encryptedDF.printSchema()
      uploadBlob(context, encryptedDF, spark, file)
    }    
    
  }
  
  def getColumns(columns : Option[Map[String, String]]) = {
    val raw = columns match {
      case Some(x) => x
      case None => Map.empty[String, String]
    }
    raw
  }
  
  def mapToMap(mutablemap: scala.collection.mutable.Map[String, String]) = {
      val immutablemap = mutablemap.foldLeft(collection.immutable.Map.empty[String, String]){(acc, m) => {
         acc + m
      }
     
     } 
       immutablemap
  }
  
 def createFileConfig(context:Context) = {
    import scala.collection.JavaConversions._
    val connections = context.connection
    val blobClient = AzureStorageHelper.getBlobClient(connections.get("source").get)  

    val container: CloudBlobContainer = AzureStorageHelper.getBlobContainer(blobClient, "sephoraprod")    
    val directory = AzureStorageHelper.getBlobDirectory(container, "inbound")//"inbound/sephora"
    val configs = directory.listBlobs().foldLeft(List.empty[FileConfig]){(acc, blobItem) => 
      val confs =  {
         if (blobItem.isInstanceOf[CloudBlob]) {
            val blob =  blobItem.asInstanceOf[CloudBlob];
            val path = "wasb://" + blob.getContainer.getName + "@" + blob.getUri.getHost + "/" + blob.getName
            val filename = blob.getName.split("/").last
            val del = FileHelper.getDelimiterFromBlob(blob)  
            val name = formatName(filename, true)
            val maskcolumns = if(context.fileconfigs.contains(name)) {
              val f = context.fileconfigs(name)
              f.maskColumns
            } else {
              None
            }
            
            val conf = FileConfig(name = filename, sourcePath = path, outputPath = "",
                stagePath ="", delimiter = del, maskColumns = maskcolumns)
            acc :+ conf
        } else {
          List.empty[FileConfig]
        }
      }
      confs
    }
    configs
  }
 
  def encryptString(s: String) = Encryption.encryptString(s)
  val encryptStringUDF = udf(encryptString _)
  
  def encryptEmail(s: String) = Encryption.encryptEmail(s) 
  val encryptEmailUDF = udf(encryptEmail _)
  
  def encryptInt(s: String) = Encryption.encryptInt(s)
  val encryptIntUDF = udf(encryptInt _)
  
  def encryptDOB(s: String) = Encryption.encryptDOB(s)
  val encryptDOBUDF = udf(encryptDOB _)  
  
  def encryptDay(s: String) = Encryption.encryptDay(s)
  val encryptDayUDF = udf(encryptDay _)  
  
  def encryptMonth(s: String) = Encryption.encryptMonth(s)
  val encryptMonthUDF = udf(encryptMonth _)  
  
  def encryptAddress(s: String) = Encryption.encryptAddress(s)
  val encryptAddressUDF = udf(encryptAddress _)  
  
  def encryptDF(maskedColumns: Option[Map[String, String]]  , dataframe: DataFrame) = {

  def mask(df:DataFrame, columnname: String,columntype:String) = {
    
    println(" ************************" + columntype + " ******************** " + columnname)
     val enc = columntype match {
         case "string" =>  df.withColumn(columnname, encryptStringUDF(col(columnname)))
         case "int" => df.withColumn(columnname, encryptIntUDF(col(columnname)))
         case "day" => df.withColumn(columnname, encryptDayUDF(col(columnname)))
         case "month" => df.withColumn(columnname, encryptMonthUDF(col(columnname)))
         case "email" => df.withColumn(columnname, encryptEmailUDF(col(columnname)))
         case "address" => df.withColumn(columnname, encryptAddressUDF(col(columnname)))
         case "dob" => df.withColumn(columnname, encryptDOBUDF(col(columnname)))
         case _ => df
     } 
     enc
   }

   def process(columns: Map[String, String], df: DataFrame): DataFrame = {    
     columns match {
       case  z if z.isEmpty =>  df
       case y if y.tail.nonEmpty => 
         {val maskedDF = mask(df, y.head._1, y.head._2)
           process(y.tail, maskedDF)}
       case x => {
         val maskedDF = mask(df, x.head._1, x.head._2)
         process(x.tail, maskedDF)
         }
     }
   }  
   
   println(maskedColumns)
   maskedColumns match {
     case Some(cols) => process(maskedColumns.get, dataframe)
     case None => dataframe
   }  
  }
  
  def downloadBlob(context: Context) = {   
    val sourceClient = AzureStorageHelper.getBlobClient(context.connection.get("source").get)  
    val container: CloudBlobContainer = AzureStorageHelper.getBlobContainer(sourceClient, "feed")    
    val directory = AzureStorageHelper.getBlobDirectory(container, "inboundobfuscation")
  }
  
  def uploadBlob(context:Context, dataframe: DataFrame, spark: SparkSession, file: FileConfig) = {   
    val connections = context.connection
      val targetClient = AzureStorageHelper.getBlobClient(connections.get("target").get)  
      val container: CloudBlobContainer = AzureStorageHelper.getBlobContainer(targetClient, "sephoraprod")    
      val directory = AzureStorageHelper.getBlobDirectory(container, "outbound/sephora/")  

      dataframe.coalesce(1).write.option("delimiter", "|").option("header", "true").mode(SaveMode.Overwrite).csv("/user/sshuser/outputobfuscated/")

      copyToLocal("", "", file,container)
  }
  
  def copyToLocal(source:String, target:String, file:FileConfig,container: CloudBlobContainer) = {
      val fs = FileSystem.get(new Configuration())
      val source = new Path("wasb://sephdspscusdstrgmet01.blob.core.windows.net/user/sshuser/outputobfuscated/")
      
      val sourceFiles = fs.listStatus(source)
      sourceFiles.foreach {f=>
        val name = FilenameUtils.getName(f.getPath.toString)
        if(!name.toLowerCase().contains("success"))
        {
           val filename = FilenameUtils.removeExtension( FilenameUtils.getName(f.getPath.toString))
           val newname = f.getPath.toString().replaceAll(filename,FilenameUtils.removeExtension(file.name))
  //         val localpath = new Path("//sephdspscusdstrgprdpi01.blob.core.windows.net/home/sshuser/temp/profile/" +  
  //             file.name + "." +  FilenameUtils.getExtension(file.name))
          val tempDir = FileUtils.getTempDirectoryPath + "/profile/"
           
          val localpath = new Path(tempDir + file.name)   //new Path("file:/home/sshuser/profile/" + file.name)
          fs.rename(f.getPath, new Path(newname)) 
          fs.copyToLocalFile(new Path(newname),localpath)
          val filepath = new File(localpath.toUri().getPath)
          val blob: CloudBlockBlob = container.getBlockBlobReference("outbound/sephora/" + file.name)
          blob.uploadFromFile(filepath.getAbsolutePath)    
          FileUtils.deleteDirectory(new File(tempDir))
          fs.delete(new Path(newname),false)
      }
    }
  }
  
  def copyBlob(context: Context) = {
     val sourceClient = AzureStorageHelper.getBlobClient(context.connection.get("source").get) 
  } 
  
  def getConnections(context:Context) = {
    context.connection
  }
  
    def formatName(input:String, isTable:Boolean = false) = {
    val formatted = if(isTable) {
      input.replaceAll("[\\-\\.]+", "_").replaceAll("[\\s\\d]+", "")
    } else {
      input.replaceAll("[\\-\\.]+", "_").replaceAll("[\\s]+", "")
    }
    
    if(formatted.trim().endsWith("__")) {
      formatted.dropRight(2)
    } else if (formatted.trim().endsWith("_")) {
       formatted.dropRight(1)
    } else {
      formatted
    }
  }
  
//  def loadLocalConfig(context: (scala.collection.mutable.Map[String, String], scala.collection.mutable.Map[String, String])) = {
//
//    val h = FileConfig(name = "filename", sourcePath = "D:\\Sephora\\PDB\\PDB\\Profile_Summary", outputPath = "",
//                stagePath ="", delimiter = "|", maskColumns = Some(context._2)) 
//      List(h)
//   }

}
