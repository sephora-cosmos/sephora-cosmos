package com.sephora.schema


import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import java.io.File

import com.sephora.schema.Entities.{Context, FileConfig}

import scala.collection.JavaConverters._
import scala.util._


object ConfigParser {
  
  def parse(path: String) = {
     val config = ConfigFactory.parseFile(new File(path)).resolve()
     
     val connectionstrings = getConnectionStrings(config)
     val sourcefiles = getSourceFiles(config)
     val fileconfigs = for(file <- sourcefiles)       
      yield {
         getFileConfigs(config, file._2.toString)
     }
    Context(connectionstrings, fileconfigs.toMap)

  }
  
  def getConnectionStrings(config : Config) = { 
      val conn = config.getObject("file-configs.connectionstrings").toConfig()
      val cons = List("source","target","stage").foldLeft(Map.empty[String, String]){(acc, con) =>
        if (conn.hasPath(con)) {
        acc + ( con -> conn.getString(con))
        } else {
          acc
        }
      }
      cons
  }
  
  def getSourceFiles(config : Config) = {

    val sourcefiles = config.getObject("file-configs.source-files").asScala.map {
      mp =>
        (mp._1, mp._2.unwrapped())
    }     
    sourcefiles
  }
  
  def getMaskColumnDetails(config: Config, filename: String) = {
    Try {
    //val schema = config.getObject("file-configs.files").asScala
    val conf = config.getConfig(s"file-configs.files.${filename}") 
    val mask = conf.getObjectList("mask-columns").asScala
    val columns = mask.map {
      mp =>
        val con = mp.toConfig()
        (con.getString("column") -> con.getString("type"))
    }
    columns.toMap
    } match {
      case Success(s) => Some(s)
      case Failure(ex) => None
    }
  }
  
  def getFileConfigs(config: Config, filename: String) = {
    val columns = getMaskColumnDetails(config, filename)

     (filename -> FileConfig(name=filename,sourcePath="",outputPath ="",stagePath="",delimiter ="",maskColumns= columns) )    
  }
}