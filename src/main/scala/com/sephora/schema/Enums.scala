package com.sephora.schema


object FileExtensionType extends Enumeration {
  type Extension = Value
  val TXT = Value("txt")
  val CSV = Value("csv")
  val TSV = Value("tsv") 
  val Parquet = Value("parquet") 
  val ORC = Value("orc") 
  val Avro =  Value("avro")
  
}