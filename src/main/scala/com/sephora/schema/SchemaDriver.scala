package com.sephora.schema




import java.io.{BufferedReader, File, InputStreamReader}

import com.microsoft.azure.storage._
import com.microsoft.azure.storage.blob._
import com.sephora.schema.Entities.FileConfig
import org.apache.commons.io.FilenameUtils
import org.apache.spark.sql.{SparkSession, _}



object SchemaDriver {
  def main(args:Array[String]) {
     val warehousePath = new File("spark-warehouse").getAbsolutePath()   
     val spark= SparkSession.builder()
          .appName("schema-generator")
          //.master("local")
          .config("spark.sql.warehouse.dir", warehousePath)
          .enableHiveSupport()
          .getOrCreate()
          
       val executors = getStorageAccountsToRun()
       for(ex <- executors) 
       {
         val fileConfig = createFileConfig(ex)
          fileConfig.foreach(f => 
          {
            val df = buildDataFrame(f, spark)
            df.show()
            df.printSchema()
            writeToHiveTables(f, df)
          })
       }     
  } 
  
  def getStorageAccountsToRun() = {
    
//    val list = List("DefaultEndpointsProtocol=https;AccountName=sephdspscusdstrgprdnpi01;AccountKey=tuPM40S8szjX/xbxkdmjf2FBFxyjHdsLB0AuMRBwgDKjKw6aMJt2fd74J7MuHy2cbga3t9QKw9X9dZlbkD1jOw==;EndpointSuffix=core.windows.net",
//        "DefaultEndpointsProtocol=https;AccountName=sephdspscusdstrgprdpi01;AccountKey=EA9l9D31wXfoMhj9FLjuHT7d1vBMitgHTFb7jSwm72oyW6yMHgc6PBapNrfvA6UillGnNhcKRyZOPiqdFVZ4ew==;EndpointSuffix=core.windows.net")
    
    val list = List("DefaultEndpointsProtocol=https;AccountName=sephdspscusdstrgpii01;AccountKey=7XBXZdTLNjGIIwn6nmtkZ5HO9WytRtuJchHzFUtJh50Ghb45dMEYTPy52fAgXejbnV0NsQYn4Lkm57Mcox6MWg==;EndpointSuffix=core.windows.net")
    list
  }
  
    private lazy val key = java.util.UUID.randomUUID.toString
    
  def getDelimiterFromBlob(inputFile: CloudBlob) = {   
      val delimiters = List("\t", "\\|", ",", "\u00FE","~")     
      val header = getFirstLineFromBlob(inputFile)
      val probableDelimiter = delimiters.foldLeft(Map.empty[Int,String]){(acc, del) => 
        {
          val schema = header.split(del)
          val formattedDel = del match {
            case "\\|" => "|"
            case _ => del
          }
          acc + (schema.length -> formattedDel)
        }
      }
      probableDelimiter.max._2
}
  
  def buildDataFrame(conf:FileConfig, spark:SparkSession) = {
    val dataframe = FilenameUtils.getExtension(conf.name).toLowerCase match {
      case "txt" | "csv" | "tsv" => {
             spark.read
             .option("header","true")
             .option("delimiter",conf.delimiter)
             .csv(conf.sourcePath)   
       }
      case "parquet" => {
         spark.read.parquet(conf.sourcePath)
      }
      case "orc" => {
        spark.read.orc(conf.sourcePath)
      }
//      case "avro" => {
//        spark.read.avro(conf.sourcePath)
//      }
      case _ => {
         spark.read
             .option("header","true")
             .option("delimiter",conf.delimiter)
             .csv(conf.sourcePath)   
      }
    }
    
    val schema = formatSchemaForHive(dataframe)  
    println("********** original df ***************")
    dataframe.printSchema()
    println("********** formatted df ***************")
    val formattedDF = dataframe.toDF(schema:_*)
    formattedDF.printSchema()
    formattedDF
  }
   
    def getFirstLineFromBlob(inputFile: CloudBlob): String = {
    val reader = new BufferedReader(new InputStreamReader(inputFile.openInputStream()))
    val firstLine = reader.readLine()
    reader.close()
    firstLine
    }
  
  def writeToHiveTables(conf:FileConfig, dataframe:DataFrame) = { 
    val db_table = getHiveTableName(conf.name)
        dataframe.write
        .format("orc")
        //.option("path","wasb:///spark-hfihsephora@hfihsephorastore.blob.core.windows.net/temp/schema/")
        //.option("path","wasb:///feed@ondemandhdinsight.blob.core.windows.net/hive/temp/schema/")
        //.partitionBy()
        //.bucketBy()
         //.option("path","wasb:///feed@hfihsephorastore.blob.core.windows.net/outbound/")
        .mode(SaveMode.Append)
        .saveAsTable(db_table)
  }
  
  def writeToBlob(conf:FileConfig,dataframe:DataFrame) = {
     dataframe.write
        .format("orc")
        .mode(SaveMode.Overwrite)
        .save(conf.stagePath)
  }
  
//  def encrypt(s: String) = Encryption.encrypt(key, s) 
//  val encryptUDF = udf(encrypt _)
//  
//  def decrypt(s: String) = Encryption.decrypt(key, s) 
//  val decryptUDF = udf(decrypt _)
// 
//  def encryptDataFrame(conf:FileConfig,dataframe:DataFrame) = {
//    
//    val df = dataframe.withColumn("E_FirstName", encryptUDF(col("FirstName")))
//    df.select(col("FirstName"),col("E_FirstName")).show()
//    df   
//  }
//    
//  def decryptDataFrame(conf:FileConfig,dataframe:DataFrame) = {   
//    val df = dataframe.select(col("*"), decryptUDF(col("E_FirstName")).alias("D_FirstName"))
//    df.select(col("FirstName"),col("E_FirstName"),col("D_FirstName")).show()
//    df   
//  }
  
  def formatSchemaForHive(dataframe:DataFrame): Seq[String] = {
    val currentSchema = dataframe.schema.fieldNames
    currentSchema.map{col =>
      formatName(col)
    }.toSeq 
  }
  
  def getHiveTableName(fileName: String, dbName: String = "db_sephora_ddr") = {
    val table = formatName(FilenameUtils.removeExtension(fileName), true)
    dbName + "." + table
    //table
  }
  
  def formatName(input:String, isTable:Boolean = false) = {
    val formatted = if(isTable) {
      input.replaceAll("[\\-\\.]+", "_").replaceAll("[\\s\\d]+", "")
    } else {
      input.replaceAll("[\\-\\.]+", "_").replaceAll("[\\s]+", "")
    }
    
    if(formatted.trim().endsWith("__")) {
      formatted.dropRight(2)
    } else if (formatted.trim().endsWith("_")) {
       formatted.dropRight(1)
    } else {
      formatted
    }
  }
  
  def createFileConfig(storageConnectionString: String = "") = {
    import scala.collection.JavaConversions._
    
    //val storageConnectionString = "DefaultEndpointsProtocol=https;AccountName=hfihsephorastore;AccountKey=8HfAEtFkdiCCantCUJu2gAytm8lqE43bzIFwbEngKU6vcw+goXh+6+/4tq01bf75ifhC/OY2aYCDGtlaP1X7OQ==;EndpointSuffix=core.windows.net"
    val storageAccount: CloudStorageAccount = CloudStorageAccount.parse(storageConnectionString)
    val blobClient = storageAccount.createCloudBlobClient()
    val container: CloudBlobContainer = blobClient.getContainerReference("sephoraprod")
    
    val directory = container.getDirectoryReference("outbound/sephora/")
    
    
    
    val configs = directory.listBlobs().foldLeft(List.empty[FileConfig]){(acc, blobItem) => 
      val confs =  {
         if (blobItem.isInstanceOf[CloudBlob]) {
            val blob =  blobItem.asInstanceOf[CloudBlob];
            val path = "wasb://" + blob.getContainer.getName + "@" + blob.getUri.getHost + "/" + blob.getName
            val filename = blob.getName.split("/").last
            val del = getDelimiterFromBlob(blob)
            val conf = FileConfig(name = filename, sourcePath = path, outputPath = "",stagePath ="", delimiter = del)
            println("********************" + conf)
            acc :+ conf
        } else {
          List.empty[FileConfig]
        }
      }
      confs
    }
    configs
  }

//  def loadConfig(path:String)  {
//    println("*************** config path ***************" + path)
//    val config = ConfigFactory.parseFile(new File(path)).resolve()
//    
//    import scala.collection.JavaConverters._
//
//    val schema = config.getObjectList("schema-configs.files").asScala.map {
//      mp =>
//        val conf = mp.toConfig
//        val filePath = conf.getString("prof_summ_eps.file_path")
//        val stagePath = conf.getString("prof_summ_eps.stage_path")
//        val outputPath = conf.getString("prof_summ_eps.output_path")
//        val filename =  Paths.get(filePath).getFileName.toString
//        //val delimiter = getDelimiter(conf)
//
//           println(filePath)
//    }    
// 
//  }


}
