package com.sephora.schema

import java.io.BufferedReader
import java.io.InputStreamReader
import com.microsoft.azure.storage.blob._



object FileHelper {
  
    def getFirstLineFromBlob(inputFile: CloudBlob): String = {
        val reader = new BufferedReader(new InputStreamReader(inputFile.openInputStream()))
        val firstLine = reader.readLine()
        reader.close()
        firstLine
    }
      
    def getDelimiterFromBlob(inputFile: CloudBlob) = { 
      val delimiters = List("\t", "\\|", ",", "\u00FE","~")     
      val header = getFirstLineFromBlob(inputFile)
      val probableDelimiter = delimiters.foldLeft(Map.empty[Int,String]){(acc, del) => 
        {
          val schema = header.split(del)
          val formattedDel = del match {
            case "\\|" => "|"
            case _ => del
          }
          acc + (schema.length -> formattedDel)
        }
      }
      probableDelimiter.max._2
}
    
}