package com.sephora.schema

import com.microsoft.azure.storage._
import com.microsoft.azure.storage.blob._



object AzureStorageHelper {

  def getBlobClient(connection: String) = {
    val storageAccount: CloudStorageAccount = CloudStorageAccount.parse(connection)
    storageAccount.createCloudBlobClient()    
  }
  
  def getBlobContainer(blobClient: CloudBlobClient, contianer: String) = {
    val container: CloudBlobContainer = blobClient.getContainerReference(contianer)
    container
  }
  
  def getBlobDirectory(container:CloudBlobContainer, directory:String) = {
     container.getDirectoryReference(directory)
  }  
}