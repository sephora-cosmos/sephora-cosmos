package com.sephora.schema



import com.sephora.schema.Entities.FileConfig
import org.apache.commons.io.FilenameUtils
import org.apache.spark.sql.SparkSession


object DataFrameHelper {
  
  def buildDataFrame(conf:FileConfig, spark:SparkSession) = {
    val dataframe = FilenameUtils.getExtension(conf.name).toLowerCase match {
      case "txt" | "csv" | "tsv" => {
             spark.read
             .option("header","true")
             .option("delimiter",conf.delimiter)
             .csv(conf.sourcePath)   
      }
      case "parquet" => {
         spark.read.parquet(conf.sourcePath)
      }
      case "orc" => {
        spark.read.orc(conf.sourcePath)
      }
      case _ => {
         spark.read
             .option("header","true")
             .option("delimiter",conf.delimiter)
             .csv(conf.sourcePath)   
      }
    }
    dataframe
  }
}