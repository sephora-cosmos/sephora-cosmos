package com.sephora.schema


object Entities {
  
  case class FileConfig(name:String, sourcePath: 
      String, outputPath: String, 
      stagePath:String, 
      delimiter: String = ",",
      outputColumns: List[String] = List.empty[String], 
      maskColumns: Option[Map[String, String]] = None)
  
  case class Context(connection : Map[String, String], fileconfigs : Map[String, FileConfig]) 
  
  
}