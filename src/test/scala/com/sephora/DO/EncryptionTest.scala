package com.sephora.DO

import org.scalatest.FunSuite
import org.scalatest.matchers._


class EncryptionTest extends  FunSuite with MustMatchers {

  test("Check for string Encryption")
  {
    val stringCheck = "sephoraSEPHORAzZ"
    Encryption.encryptString(stringCheck) must be("ugrjqtcUGRJQTCBc")
  }

  test("Check for int Encryption")
  {
    val intCheck = "123456789"
    //Encryption.encryptString(stringCheck) must be("ugrjqtcUGRJQTCBc")
    println(Encryption.encryptString(intCheck))
  }

  test("Check for dob Encryption")
  {
    val stringCheck = "sephoraSEPHORAzZ"
    Encryption.encryptString(stringCheck) must be("ugrjqtcUGRJQTCBc")
  }

  test("Check for month Encryption")
  {
    val stringCheck = "sephoraSEPHORAzZ"
    Encryption.encryptString(stringCheck) must be("ugrjqtcUGRJQTCBc")
  }
  test("Check for date Encryption")
  {
    val stringCheck = "sephoraSEPHORAzZ"
    Encryption.encryptString(stringCheck) must be("ugrjqtcUGRJQTCBc")
  }

  test("Check for email Encryption")
  {
    val stringCheck = "sephoraSEPHORAzZ"
    Encryption.encryptString(stringCheck) must be("ugrjqtcUGRJQTCBc")
  }
  test("Check for address Encryption")
  {
    val stringCheck = "sephoraSEPHORAzZ"
    Encryption.encryptString(stringCheck) must be("ugrjqtcUGRJQTCBc")
  }
}
